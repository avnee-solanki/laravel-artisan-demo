<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TeamGameWonResultTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testTeamAWon()
    {
        $this->artisan('game:play')
         ->expectsQuestion('Enter Team-A Players:', '30, 100, 20, 50, 40')
         ->expectsQuestion('Enter Team-B Players:', '35, 10, 30, 20, 90')
         ->expectsOutput('Win')
         ->assertExitCode(0);
    }
}
