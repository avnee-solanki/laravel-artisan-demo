<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TeamGameLoseResultTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testTeamALose()
    {
        $this->artisan('game:play')
         ->expectsQuestion('Enter Team-A Players:', '35, 10, 30, 20, 90')
         ->expectsQuestion('Enter Team-B Players:', '30, 100, 20, 50, 40')
         ->expectsOutput('Lose')
         ->assertExitCode(0);
    }
}
