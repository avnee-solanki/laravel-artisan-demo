<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TeamInputInvalidTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testInValidInputForTeamA()
    {
        $this->artisan('game:play')
         ->expectsQuestion('Enter Team-A Players:', '10, a, 12')
         ->expectsQuestion('Enter Team-B Players:', '35, 10, 30, 20, 90')
         ->expectsOutput('Drain value must be greater than 0 for every player in a team')
         ->assertExitCode(0);
    }
}
