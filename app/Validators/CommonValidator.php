<?php
declare(strict_types = 1);

namespace App\Validators;

use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Validator as InputValidator;

/**
 * Class CommonValidator
 *
 * @package App\Validators
 */
class CommonValidator extends Validator
{

    /**
     * Validates string, breaks using comma and checks the length of array. Elements should be of int type and greater than 0
     * Accessed as "break_value_and_check"
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    protected function validateBreakValueAndCheck($attribute, $value): bool
    {
        $valueAsArray = array_map('intval', explode(',', $value));
        $arrayElementsCounts = array_count_values($valueAsArray);

        if (count($valueAsArray) != 5) {
            $this->errors()->add($attribute, 'There must be exactly 5 players in a team');

            return false;
        }

        if (isset($arrayElementsCounts[0]) && $arrayElementsCounts[0] > 0) {
            $this->errors()->add($attribute, 'Drain value must be greater than 0 for every player in a team');

            return false;
        }

        return true;
    }
}
