<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Validator;

abstract class BaseCommand extends Command
{

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Modified the base method ask() with validation support to inputs of command.
     *
     * @param string $question
     * @param string $fieldName
     * @param array $rules
     * @return string $value
     */
    protected function askValid(string $question, string $fieldName, array $rules): string
    {
        $value = $this->ask($question);

        if ($message = $this->validateInput($rules, $fieldName, $value)) {

            $this->error($message);

            return $this->askValid($question, $fieldName, $rules);
        }

        return $value;
    }

    /**
     * Validates the input and returns the errors if any else null
     *
     * @param array $rules
     * @param string $fieldName
     * @param string $value
     * @return string|null ERROR
     */
    private function validateInput(array $rules, string $fieldName, string $value): ?string
    {
        $validator = Validator::make([
            $fieldName => $value
        ], [
            $fieldName => $rules
        ]);

        return $validator->fails() ? $validator->errors()->first($fieldName) : null;
    }
}
