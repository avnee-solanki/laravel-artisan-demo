<?php

namespace App\Console\Commands\Game;

use App\Console\Commands\BaseCommand;

class PlayGame extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     */
    protected $signature = 'game:play';

    /**
     * The console command description.
     *
     */
    protected $description = 'Command takes Team A & Team B players drain level data as input, separated by comma and declares result for Team A';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        try {
            $teamAInput = $this->askValid(
                "Enter Team-A Players:",
                "team_a_players",
                ['required','break_value_and_check'],
                ['required','break_value_and_check']
            );

            $teamBInput = $this->askValid(
                "Enter Team-B Players:",
                "team_b_players",
                ['required','break_value_and_check:team_a_players']
            );

            $teamAPlayers = array_map('intval', explode(',', $teamAInput));
            $teamBPlayers = array_map('intval', explode(',', $teamBInput));

            sort($teamAPlayers);
            sort($teamBPlayers);

            $lose = false;
            foreach ($teamBPlayers as $key => $value) {
                if ($value >= $teamAPlayers[$key]) {
                    $lose = true;
                    break;
                }
            }
            if ($lose) {
                $this->info("Lose");
            } else {
                $this->info("Win");
            }
        } catch (Exception $e) {
            $this->error("Error::".$e->getMessage());
        }
    }
}
