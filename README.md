# Laravel Artisan Demo

## Description
[ read description here : validate inputs and find winning team](puzzle.md)

## Getting started with set up

### Requirements
- PHP >= 8.0
- Composer

### Install Composer

[download and install composer](https://getcomposer.org/download/)

### Clone Repository
open your terminal, go to the directory that you will install this project, then run the following command:

```bash
git clone https://gitlab.com/avnee-solanki/laravel-artisan-demo.git

cd laravel-artisan-demo

composer install

php artisan game:play
```
